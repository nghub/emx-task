import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UsersFacade } from '../root-store/users-store/users.facade';
import { UserInterface } from '../shared/models/user.model';
import { Subject, takeUntil } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['id', 'name', 'email', 'phone', 'website'];
  dataSource: any;
  userData$ = this.usersFacade.getUserData$;
  isUserLoading$ = this.usersFacade.isUsersLoading$;
  private unsubscribe$ = new Subject<void>();

  @ViewChild(MatPaginator) paginatore!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private usersFacade: UsersFacade) { }

  ngOnInit(): void {
    this.usersFacade.getUsers();    
    this.userData$
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(users => {
      this.dataSource = new MatTableDataSource<UserInterface>(users);
      this.dataSource.paginator = this.paginatore;
      this.dataSource.sort = this.sort;
    });
  
  }

  filterChange(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
