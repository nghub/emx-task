import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UsersComponent } from './users.component';
import { UsersFacade } from '../root-store/users-store/users.facade';
import { MaterialComponentsModule } from '../shared/material-components/material-components.module';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  beforeEach(async() => {
    await TestBed.configureTestingModule({
      declarations: [UsersComponent],
      imports: [MaterialComponentsModule],
      providers: [
        { provide: UsersFacade, useValue: jasmine.createSpyObj('UsersFacade', ['']) },
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


  