import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserInterface } from '../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getusers(): Observable<UserInterface[]> {
    console.log('User service Trigger');
    return this.http.get<UserInterface[]>('/users');
  }
}
