import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';

import { LoginComfirmationComponent } from './login-message.component';

describe('LoginComfirmationComponent', () => {
  let component: LoginComfirmationComponent;
  let fixture: ComponentFixture<LoginComfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComfirmationComponent ],
      providers: [
        { provide: MatDialogRef, useValue: jasmine.createSpyObj('MatDialogRef', [''])}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
