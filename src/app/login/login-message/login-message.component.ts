import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-login-message',
  templateUrl: './login-message.component.html',
  styleUrls: ['./login-message.component.scss']
})
export class LoginComfirmationComponent {

  constructor(public dialogRef: MatDialogRef<LoginComfirmationComponent>) { }

  onCancel() {
    this.dialogRef.close({ action: false});
  }

}
