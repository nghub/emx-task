import { Component, OnDestroy, OnInit } from '@angular/core';
import { SessionFacade } from '../root-store/session-store/session.facade';
import { MatDialog } from '@angular/material/dialog';
import { LoginComfirmationComponent } from './login-message/login-message.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm!: FormGroup;
  error$ = this.sessionFacade.error$;
  loginInProgress$ = this.sessionFacade.loginInProgress$;
  private unsubscribe$ = new Subject<void>();

  user: SocialUser | null = null;

  constructor(
    private fb: FormBuilder, 
    private sessionFacade: SessionFacade,
    private dialog: MatDialog,
    private authService: SocialAuthService ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });

    this.authService?.authState
    ?.pipe(takeUntil(this.unsubscribe$))
    ?.subscribe((user) => {
      if(user) {
        this.user = user;
        this.sessionFacade.socialLoginSuccess(user);
      }
    });
  }

  get username() { return this.loginForm.get('username'); }

  get password() { return this.loginForm.get('password'); }

  onSubmit() {
    this.dialog.open(LoginComfirmationComponent)
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
