import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { SessionFacade } from '../root-store/session-store/session.facade';
import { MaterialComponentsModule } from '../shared/material-components/material-components.module';
import { GoogleSigninButtonModule, SocialAuthService } from '@abacritt/angularx-social-login';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  selector: 'asl-google-signin-button',
  template: '',
})
export class MockGoogleButtonComponent {}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  
  beforeEach(async() => {
    await TestBed.configureTestingModule({
      declarations: [
        LoginComponent,
        MockGoogleButtonComponent
      ],
      imports: [
        MaterialComponentsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: SessionFacade, useValue: jasmine.createSpyObj('SessionFacade', [''])},
        { provide: SocialAuthService, useValue: jasmine.createSpyObj('SocialAuthService', [''])},
        { provide: GoogleSigninButtonModule, useValue: jasmine.createSpyObj('GoogleSigninButtonModule', [''])}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render login form title', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.login-header')?.textContent).toContain('User login');
  });


});

