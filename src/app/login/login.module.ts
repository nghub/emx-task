import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { MaterialComponentsModule } from '../shared/material-components/material-components.module';
import { LoginComfirmationComponent } from './login-message/login-message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleSigninButtonModule } from '@abacritt/angularx-social-login';

@NgModule({
  declarations: [
    LoginComponent,
    LoginComfirmationComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialComponentsModule,
    GoogleSigninButtonModule
  ],
  providers: []
})
export class LoginModule { }
