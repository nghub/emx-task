import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SessionFacade } from './root-store/session-store/session.facade';
import { MaterialComponentsModule } from './shared/material-components/material-components.module';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';

@Component({
  selector: 'app-top-menu',
  template: ''
})
export class MockTopMenuComponent {}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let sessionFacade: jasmine.SpyObj<SessionFacade>;

  beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, MaterialComponentsModule],
        declarations: [AppComponent, MockTopMenuComponent],
        providers: [
          { provide: SessionFacade, useValue: jasmine.createSpyObj('SessionFacade', [''])},
        ]
      });

      fixture = TestBed.createComponent(AppComponent);
      component = fixture.componentInstance;
      sessionFacade = TestBed.inject(SessionFacade) as jasmine.SpyObj<SessionFacade>;
    }
  
  );

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'practice'`, () => {
    const app = fixture.componentInstance;
    expect(app.title).toEqual('practice');
  });

  it('should have router', () => {
    fixture.detectChanges();
    const de = fixture.debugElement.query(By.css('router-outlet'));    
    expect(de).toBeTruthy();
  });

  it('Should have isUserLoggedIn$ false by default', () => {
    sessionFacade.isUserLoggedIn$ = of(false);
    fixture.detectChanges();
    component.isUserLoggedIn$.subscribe(isLogin => {
      expect(isLogin).toBeFalse()
    })
  });

  it('Should have isUserLoggedIn$ true after login', () => {
    sessionFacade.isUserLoggedIn$ = of(true);
    fixture.detectChanges();
    component.isUserLoggedIn$.subscribe(isLogin => {
      expect(isLogin).toBeTrue()
    })
  });

});
