import { CanActivateFn } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionFacade } from '../root-store/session-store/session.facade';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state): Observable<boolean> => {
  const service = inject(SessionFacade);
  return service.isUserLoggedIn$;
};
