export interface UserInterface {
    id: string,
    name: string,
    email: string,
    username: string,
    phone: string,
    website: string,
    company: any,
    address: any
  }
