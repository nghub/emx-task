import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore  } from '@ngrx/store/testing';

import { TopMenuComponent } from './top-menu.component';
import { SessionFacade } from '../root-store/session-store/session.facade';

import { SocialAuthService } from '@abacritt/angularx-social-login';
import { MaterialComponentsModule } from '../shared/material-components/material-components.module';

describe('TopMenuComponent', () => {
  let component: TopMenuComponent;
  let fixture: ComponentFixture<TopMenuComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TopMenuComponent],
      imports: [MaterialComponentsModule],
      providers: [
        provideMockStore(),
        { provide: SessionFacade, useValue: jasmine.createSpyObj('SessionFacade', [''])},
        { provide: SocialAuthService, useValue: jasmine.createSpyObj('SocialAuthService', [''])}
      ]
    });
    fixture = TestBed.createComponent(TopMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
