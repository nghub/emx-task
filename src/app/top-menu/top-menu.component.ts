import { Component } from '@angular/core';
import { SessionFacade } from '../root-store/session-store/session.facade';
import { Observable } from 'rxjs';
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent {
  userData$: Observable<SocialUser | null> = this.sessionFacade.getUserData$;

  constructor(
    private sessionFacade: SessionFacade,
    private authService: SocialAuthService
    ) { }


  onLogout() {
    this.authService.signOut();
    this.sessionFacade.performLogout();
  }
}
