import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { SessionFacade } from './root-store/session-store/session.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'practice';

  isUserLoggedIn$!: Observable<boolean>;

  constructor(private sessionFacade: SessionFacade) { }

  ngOnInit(): void {
    this.isUserLoggedIn$ = this.sessionFacade.isUserLoggedIn$;
  }


   
}
