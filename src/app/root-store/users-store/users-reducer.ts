import { Action, createReducer, on } from "@ngrx/store";
import { UsersState, initialState } from "./users-state";
import { getUsers, getUsersFail, getUsersSuccess } from "./users-action";

const reducers =createReducer(
    initialState,
    on(getUsers, (state): UsersState => {
        return {
            ...state,
            isUsersLoading: true
        }
    }),
    on(getUsersSuccess, (state, { users }): UsersState => {
        console.log('getUsersSuccess Reducer ', users);
        return {
            ...state,
            usersData: users,
            isUsersLoading: false
        }
    }),
    on(getUsersFail, (state, {error}): UsersState => {
        console.log('getUsersFail');
        return {
            ...state,
            isUsersLoading: false,
            usersData: [],
            error: error
        }
    })
    
);

export function usersReducer(state: UsersState | undefined, action: Action) {
    return reducers(state, action);
}