import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { UserService } from "src/app/services/user.service";
import { getUsers, getUsersFail, getUsersSuccess } from "./users-action";
import { catchError, map, mergeMap, of } from "rxjs";

@Injectable()
export class UsersEffect {
    getUsersEffect = createEffect(() => 
    this.action$.pipe(
        ofType(getUsers),
        mergeMap(() => {
            console.log('UsersEffect');
            return this.userService.getusers().pipe(
                map(
                    (users) => getUsersSuccess({ users })
                ),
                catchError((error) => {
                    console.log('catchError ', error);
                    return of(getUsersFail( { error: error.message }))
                })
            )
        })
    )
    )

    constructor(private action$: Actions, 
        private userService: UserService) {}
}