import { UserInterface } from "src/app/shared/models/user.model"

export interface UsersState {
    isUsersLoading: boolean,
    error: string | null,
    usersData: UserInterface[]
}

export const initialState: UsersState = {
    isUsersLoading: false,
    error: null,
    usersData: []
}