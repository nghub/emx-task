import { createFeatureSelector, createSelector } from "@ngrx/store";
import { UsersState } from "./users-state";
import { UserInterface } from "src/app/shared/models/user.model";

export const selectUsersState = createFeatureSelector<UsersState>('users');

export const selectIsUsersLoading = createSelector(
    selectUsersState,
    (state: UsersState) : boolean => state.isUsersLoading
);

export const selectUsers = createSelector(
    selectUsersState,
    (state: UsersState): UserInterface[] => state.usersData
);

export const selectUsersError = createSelector(
    selectUsersState,
    (state: UsersState): string | null => state.error
);
