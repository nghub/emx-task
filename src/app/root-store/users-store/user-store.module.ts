import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {  StoreModule } from "@ngrx/store";
import { usersReducer } from "./users-reducer";
import { UsersEffect } from "./users.effect";
import { EffectsModule } from "@ngrx/effects";
import { UsersFacade } from "./users.facade";

@NgModule({
    declarations: [],
    imports:[
        CommonModule,
        StoreModule.forFeature('users', usersReducer),
        EffectsModule.forFeature([UsersEffect, UsersFacade])
    ]
})
export class UsersStoreModule { }