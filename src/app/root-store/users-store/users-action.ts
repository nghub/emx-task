import { createAction, props } from '@ngrx/store';
import { UserInterface } from '../../shared/models/user.model';

const GET_USERS = '[Users] get USers';
const GET_USERS_SUCCESS = '[Users] get USers success';
const GET_USERS_FAIL = '[Users] get USers Fails';

export const getUsers = createAction(GET_USERS);
export const getUsersSuccess = createAction(GET_USERS_SUCCESS, 
    props<{ users: UserInterface[]}>()
);

export const getUsersFail = createAction(GET_USERS_FAIL, 
    props<{ error: string }>()
);