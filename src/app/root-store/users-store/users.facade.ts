import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../root-state';
import { getUsers } from './users-action';
import { selectIsUsersLoading, selectUsers, selectUsersError } from './users-selector';

@Injectable()
export class UsersFacade {
  isUsersLoading$ = this.store.select(selectIsUsersLoading);
  getUserData$ = this.store.select(selectUsers);
  getusersError$ = this.store.select(selectUsersError);

  constructor(private store: Store<RootState>) { }

  getUsers() {
    this.store.dispatch(getUsers());
  }

}
