import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { sessionReducer } from './session.reducers';
import { SessionStoreEffects } from './session.effects';
import { SessionFacade } from './session.facade';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('session', sessionReducer),
    EffectsModule.forFeature([SessionStoreEffects])
  ],
  providers: [SessionStoreEffects, SessionFacade]
})
export class SessionStoreModule { }
