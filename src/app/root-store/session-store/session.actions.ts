import { createAction, props } from '@ngrx/store';
import { SocialUser } from '@abacritt/angularx-social-login';


const SOCIAL_LOGIN_SUCCESS = '[Session] Social Login Success';
const SOCIAL_LOGIN_FAILURE = '[Session] Social Failure';
const LOGOUT = '[Session] Logout';

export const socialLoginSuccess = createAction(SOCIAL_LOGIN_SUCCESS, props<{ user: SocialUser}>());
export const socialLoginFail = createAction(SOCIAL_LOGIN_FAILURE, props<{ payload: any}>()); // TODO: TO check type of error while fail

export const performLogout = createAction(LOGOUT);
