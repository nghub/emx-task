import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectIsUserLoggedIn, selectAccessToken, selectSessionError, selectLoginInProgress, selectUserData } from './session.selectors';
import { performLogout, socialLoginSuccess } from './session.actions';
import { RootState } from '../root-state';
import { SocialUser } from '@abacritt/angularx-social-login';

@Injectable()
export class SessionFacade {
  accessToken$ = this.store.select(selectAccessToken);
  isUserLoggedIn$ = this.store.select(selectIsUserLoggedIn);
  error$ = this.store.select(selectSessionError);
  loginInProgress$ = this.store.select(selectLoginInProgress);
  getUserData$ = this.store.select(selectUserData);

  constructor(private store: Store<RootState>) { }

  socialLoginSuccess(user: SocialUser) {
    this.store.dispatch(socialLoginSuccess({user}));
  }

  performLogout() {
    this.store.dispatch(performLogout());
  }

}
