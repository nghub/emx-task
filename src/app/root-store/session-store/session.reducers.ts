import { SessionState, initialState } from './session.state';
import {
  socialLoginSuccess,
  performLogout,
} from './session.actions';
import { Action, createReducer, on } from '@ngrx/store';

const reducer = createReducer(
  initialState,
  on(performLogout, (state): SessionState => {
    return {
      ...state,
      accessToken: null,
      error: null,
      userData: null,
      isUserLoggedIn: false
    };
  }),
  on(socialLoginSuccess, (state, { user }): SessionState => {
    return {
      ...state,
      accessToken: user.authToken,
      userData: user,
      isUserLoggedIn: true
    };
  }),
);

export function sessionReducer(state: SessionState | undefined, action: Action) {
  return reducer(state, action);
}
