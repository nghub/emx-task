import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  performLogout, socialLoginSuccess
} from './session.actions';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { RootState } from '../root-state';
import { clearStateAction } from '../root.actions';

@Injectable()
export class SessionStoreEffects {

  constructor(
    private actions$: Actions,
    private router: Router,
    private store$: Store<RootState>
  ) { }
  
  socailLoginSuccessEffect$ = createEffect(() => this.actions$.pipe(
    ofType(socialLoginSuccess),
    tap(() => {
      //TODO console.log('API needed to share the auth token to Server >> ', user);
      this.router.navigate(['dashboard']);
    })
  ), { dispatch: false });

  logoutEffect$ = createEffect(() => this.actions$.pipe(
    ofType(performLogout),
    tap(() => {
      this.router.navigate(['/login']).then(() => {
        this.store$.dispatch(clearStateAction());
        sessionStorage.clear();
      })
    })
  ),{ dispatch: false });
}
