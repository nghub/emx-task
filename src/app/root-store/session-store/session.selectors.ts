import { createFeatureSelector, createSelector } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { SessionState } from './session.state';
import { SocialUser } from '@abacritt/angularx-social-login';

export const selectSessionState = createFeatureSelector<SessionState>('session');

export const selectIsUserLoggedIn = createSelector(
  selectSessionState,
  (state: SessionState): boolean => state.isUserLoggedIn
);

export const selectAccessToken = createSelector(
  selectSessionState,
  (state: SessionState): string | null => state.accessToken
);

export const selectSessionError = createSelector(
  selectSessionState,
  (state: SessionState): HttpErrorResponse | null => state.error
);

export const selectLoginInProgress = createSelector(
  selectSessionState,
  (state: SessionState): boolean => state.loginInProgress
);


export const selectUserData = createSelector(
  selectSessionState,
  (state: SessionState): SocialUser | null => state.userData
);