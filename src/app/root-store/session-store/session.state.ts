import { SocialUser } from '@abacritt/angularx-social-login';
import { HttpErrorResponse } from '@angular/common/http';

export interface SessionState {
  isUserLoggedIn: boolean;
  accessToken: string | null;
  userData: SocialUser | null,
  error: HttpErrorResponse | null;
  loginInProgress: boolean;
}

export const initialState: SessionState = {
  isUserLoggedIn: false,
  accessToken: null,
  userData: null,
  error: null,
  loginInProgress: false
};
