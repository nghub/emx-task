import { SessionState } from "./session-store/session.state";
import { UsersState } from "./users-store/users-state";


export interface RootState {
    users: UsersState,
    session: SessionState
}