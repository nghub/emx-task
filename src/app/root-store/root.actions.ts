import {createAction} from '@ngrx/store';

const CLEAR_STATE = '[Root] Clear State';

export const clearStateAction = createAction(CLEAR_STATE);
