import { CommonModule } from "@angular/common";
import { InjectionToken, NgModule } from "@angular/core";
import { ActionReducer, ActionReducerMap, MetaReducer, StoreModule } from "@ngrx/store";
import { UsersStoreModule } from "./users-store/user-store.module";
import { RootState } from "./root-state";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { storageSync } from "@larscom/ngrx-store-storagesync";
import { clearStateAction } from "./root.actions";
import { EffectsModule } from "@ngrx/effects";
import { SessionStoreModule } from "./session-store/session-store.module";


export const ROOT_REDUCER = new InjectionToken<ActionReducerMap<RootState>>('Root Reducer');

export function storageSyncReducer(reducer: ActionReducer<RootState>): ActionReducer<RootState> {
    return storageSync<RootState>({
      version: 1,
      features: [
        {
          stateKey: 'login',
          excludeKeys: ['error', 'loginInProgress']
        },
        { stateKey: 'users' },
        { stateKey: 'session' }
      ],
      storageError: console.error,
      storage: window.sessionStorage
    })(reducer);
  }
  
  export function clearState(reducer: ActionReducer<any>): ActionReducer<any> {
    return (state, action) => {
      if (action.type === clearStateAction.type) {
        state = undefined;
      }
      return reducer(state, action);
    };
  }
  
  export const metaReducers: MetaReducer<RootState>[] = [clearState, storageSyncReducer];
  

@NgModule({
    declarations: [],
    imports:[
        CommonModule,
        StoreModule.forRoot(ROOT_REDUCER, { metaReducers }),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
          }),
        UsersStoreModule,
        SessionStoreModule
    ],
    providers: [{ provide: ROOT_REDUCER, useValue: {}}]
})
export class RootStoreModule { }